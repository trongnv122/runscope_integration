import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private http: HttpClient,
              private route: ActivatedRoute) {

  }

  title = 'app works!';
  clientId: string = '34e4db1c-3071-46b2-b81c-d5504b50a013';
  clientSecret: string = '36f1eec2-9d53-4027-804f-8e0da78fbdb5';
  callBackUrl: string = 'http://localhost:4200';
  authTokenUrl: string = 'https://www.runscope.com/signin/oauth/access_token?' +
    'client_id=' + this.clientId +
    '&client_secret=' + this.clientSecret +
    '&code=AUTH_CODE' +
    '&grant_type=authorization_code' +
    '&redirect_uri=http://localhost:4200'

  getState = function () {
    return new Date().getTime()
  }

  authorizeURL: string = 'https://www.runscope.com/signin/oauth/authorize?' +
    'client_id=' + this.clientId +
    '&redirect_uri=http://localhost:4200' +
    '&response_type=code' +
    '&scope=api:read%20messsage:write' +
    '&state=';

  getAuthorizeURL() {
    return this.authorizeURL + this.getState();
  }

  refreshToken: string = '';
  token: string = '';

  auth(code: string, state: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa('user1:password1')
      })
    };

    let body = new URLSearchParams();
    // body.set('client_id', this.clientId);
    // body.set('client_secret', this.clientSecret);
    body.set('code', code);
    body.set('state', state);
    // body.set('grant_type', 'authorization_code');
    // body.set('redirect_uri', 'http://localhost:4200');

    this.http.post<any>('http://localhost:8506/runscope/auth-callback', body.toString(), httpOptions)
      .pipe(
      ).subscribe({
      next: (v) => console.log("NEXT >>>>>>> ", v),
      error: (e) => console.error("ERROR >>>>>>> ", e),
      complete: () => console.info("COMPLETE >>>>>>> ",'complete')
    })
  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
          console.log("params >>>>>>>>>>>>>>>>> " + JSON.stringify(params));

          if (params['code'] && params['state']) {
            this.auth(params['code'], params['state']);
          }
        }
      );
  }
}
