# RUN SCOPE PROJECT

Update your environment variables with .env file.

RUN_SCOPE_CLIENT_ID=xxxxx
RUN_SCOPE_CLIENT_SECRET=xxxxxx
RUN_SCOPE_URL=https://www.runscope.com
REDIS_HOST=redis-0
REDIS_PORT=6379
