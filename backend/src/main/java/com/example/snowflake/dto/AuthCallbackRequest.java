package com.example.snowflake.dto;

import lombok.Data;

@Data
public class AuthCallbackRequest {
    private String state;
    private String code;
}
