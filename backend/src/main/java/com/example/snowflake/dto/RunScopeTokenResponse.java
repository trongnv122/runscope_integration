package com.example.snowflake.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class RunScopeTokenResponse implements Serializable {
    @JsonProperty("access_token")
    private String token;
    @JsonProperty("token_type")
    private String type;
}
