package com.example.snowflake;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class RunScopeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RunScopeApplication.class, args);
	}

}
