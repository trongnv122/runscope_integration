package com.example.snowflake.service;

public interface RunScopeService {
    void auth(String code, String state);
}
