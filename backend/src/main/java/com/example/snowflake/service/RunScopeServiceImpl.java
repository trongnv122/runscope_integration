package com.example.snowflake.service;

import com.example.snowflake.dto.RunScopeTokenResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class RunScopeServiceImpl implements RunScopeService {
    private final RedisTemplate<String, Object> redisTemplate;

    RestTemplate restTemplate = new RestTemplate();

    @Value("${runscope.client.id}")
    private String runscopeClientId;
    @Value("${runscope.client.secret}")
    private String runscopeClientSecret;

    //https://www.runscope.com
    @Value("${runscope.url}")
    private String runscopeBaseURL;

    public RunScopeServiceImpl(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void auth(String code, String state) {
        String AUTH_URL = runscopeBaseURL + "/signin/oauth/access_token";

        System.out.println("AUTH_URL >>> " + AUTH_URL);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", runscopeClientId);
        map.add("client_secret", runscopeClientSecret);
        map.add("code", code);
        map.add("state", state);
        map.add("grant_type", "authorization_code");
        map.add("redirect_uri", "http://localhost:4200");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<RunScopeTokenResponse> response = restTemplate.postForEntity(AUTH_URL, request, RunScopeTokenResponse.class);

        if (response.getBody() != null) {
            redisTemplate.opsForValue().set("run_scope_token", response.getBody());
        }
    }
}
