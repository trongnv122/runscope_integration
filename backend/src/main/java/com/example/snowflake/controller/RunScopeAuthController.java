package com.example.snowflake.controller;

import com.example.snowflake.dto.AuthCallbackRequest;
import com.example.snowflake.service.RunScopeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/runscope")
public class RunScopeAuthController {
    @Autowired
    private RunScopeService runScopeService;

    @PostMapping(
            path = "/auth-callback",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<?> auth(AuthCallbackRequest request) {
        runScopeService.auth(request.getCode(), request.getState());
        return ResponseEntity.ok(Boolean.TRUE);
    }
}
