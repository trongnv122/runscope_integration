package com.example.snowflake.service;

import com.example.snowflake.dto.RunScopeTokenResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;


@SpringBootTest
@SpringBootConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = {
        "runscope.url=https://www.runscope.com",
        "runscope.client.id=9d671571a",
        "runscope.client.secret=ffa59da5a35d83e34",
})
public class RunScopeServiceTest {
    @Value("${runscope.client.id}")
    String runscopeClientId;
    @Value("${runscope.client.secret}")
    String runscopeClientSecret;
    @Value("${runscope.url}")
    String runscopeBaseURL;

    @Mock RestTemplate restTemplate;

    @Mock RedisTemplate<String, Object> redisTemplate;

    @Captor ArgumentCaptor<HttpEntity<MultiValueMap<String, String>>> callRestCaptor;

    RunScopeServiceImpl runScopeService = new RunScopeServiceImpl(redisTemplate);

    @Before
    public void before () {
        ReflectionTestUtils.setField(runScopeService, "runscopeClientId", runscopeClientId);
        ReflectionTestUtils.setField(runScopeService, "runscopeClientSecret", runscopeClientSecret);
        ReflectionTestUtils.setField(runScopeService, "runscopeBaseURL", runscopeBaseURL);
        ReflectionTestUtils.setField(runScopeService, "restTemplate", restTemplate);
        ReflectionTestUtils.setField(runScopeService, "redisTemplate", redisTemplate);
    }
    @Test
    public void testAuth() {
        // Mock
        String code = "code_for_test";
        String state = "state_for_test";

        @SuppressWarnings("unchecked")
        ResponseEntity<RunScopeTokenResponse> response = Mockito.mock(ResponseEntity.class);

        Mockito.when(restTemplate.postForEntity(
                eq("https://www.runscope.com/signin/oauth/access_token"),
                callRestCaptor.capture(),
                eq(RunScopeTokenResponse.class))
        ).thenReturn(response);

        RunScopeTokenResponse tokenResponse = new RunScopeTokenResponse();
        Mockito.when(response.getBody()).thenReturn(tokenResponse);

        @SuppressWarnings("unchecked")
        ValueOperations<String, Object> valueOperations = Mockito.mock(ValueOperations.class);

        Mockito.when(redisTemplate.opsForValue()).thenReturn(valueOperations);

        //Test call real
        runScopeService.auth(code, state);
        HttpEntity<MultiValueMap<String, String>> request = callRestCaptor.getValue();
        MultiValueMap<String, String> payload = request.getBody();

        //Assert call rest with params
        assertNotNull(payload);
        Map<String, String> keyValues = payload.toSingleValueMap();
        assertEquals(runscopeClientId, keyValues.get("client_id"));
        assertEquals(runscopeClientSecret, keyValues.get("client_secret"));
        assertEquals("authorization_code", keyValues.get("grant_type"));
        assertEquals(code, keyValues.get("code"));
        assertEquals(state, keyValues.get("state"));

        //assert call redis to store value
        Mockito.verify(redisTemplate.opsForValue(), times(1)).set(eq("run_scope_token"), any());
    }
}
